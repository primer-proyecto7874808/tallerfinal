import heapq
import networkx as nx
import matplotlib.pyplot as plt

class Pila:
    def __init__(self):
        self.items = []
        # Inicializa una lista vacía para almacenar los elementos de la pila.

    def esta_vacia(self):
        return len(self.items) == 0
        # Devuelve True si la pila está vacía, False en caso contrario.

    def apilar(self, item):
        self.items.append(item)
        print(f"{item} ha sido apilado.")
        # Agrega un elemento a la pila y muestra un mensaje indicando que ha sido apilado.

    def desapilar(self):
        if self.esta_vacia():
            return "La pila está vacía, no se puede desapilar."
        return self.items.pop()
        # Elimina y devuelve el último elemento de la pila. Si la pila está vacía, devuelve un mensaje de error.

    def tope(self):
        if self.esta_vacia():
            return "La pila está vacía."
        return self.items[-1]
        # Devuelve el último elemento de la pila sin eliminarlo. Si la pila está vacía, devuelve un mensaje de error.

    def tamano(self):
        return len(self.items)
        # Devuelve el número de elementos en la pila.

class Cola:
    def __init__(self):
        self.items = []
        # Inicializa una lista vacía para almacenar los elementos de la cola.

    def esta_vacia(self):
        return len(self.items) == 0
        # Devuelve True si la cola está vacía, False en caso contrario.

    def encolar(self, item):
        self.items.insert(0, item)
        print(f"{item} ha sido encolado.")
        # Agrega un elemento al inicio de la cola y muestra un mensaje indicando que ha sido encolado.

    def desencolar(self):
        if self.esta_vacia():
            return "La cola está vacía, no se puede desencolar."
        return self.items.pop()
        # Elimina y devuelve el último elemento de la cola. Si la cola está vacía, devuelve un mensaje de error.

    def tamano(self):
        return len(self.items)
        # Devuelve el número de elementos en la cola.

class Nodo:
    def __init__(self, llave):
        self.izquierda = None
        self.derecha = None
        self.valor = llave
        # Inicializa un nodo con una clave (llave) y referencias nulas para los nodos hijos izquierdo y derecho.

def insertar(root, llave):
    if root is None:
        return Nodo(llave)
    else:
        if root.valor < llave:
            root.derecha = insertar(root.derecha, llave)
        else:
            root.izquierda = insertar(root.izquierda, llave)
    return root
    # Inserta una nueva clave en el árbol binario de búsqueda (BST). Si el árbol está vacío, crea un nuevo nodo raíz.
    # De lo contrario, recorre el árbol recursivamente para encontrar la posición correcta para la nueva clave.

def buscar(root, llave):
    if root is None or root.valor == llave:
        return root
    if root.valor < llave:
        return buscar(root.derecha, llave)
    return buscar(root.izquierda, llave)
    # Busca una clave en el BST. Si encuentra la clave, devuelve el nodo correspondiente; de lo contrario, devuelve None.

def inorden(root):
    return inorden(root.izquierda) + [root.valor] + inorden(root.derecha) if root else []
    # Realiza un recorrido en orden (in-order traversal) del BST y devuelve una lista de las claves en orden ascendente.

def bfs(grafo, inicio):
    visitados = []
    cola = [inicio]

    while cola:
        nodo = cola.pop(0)
        if nodo not in visitados:
            visitados.append(nodo)
            vecinos = grafo[nodo]

            for vecino in vecinos:
                cola.append(vecino)
    
    return visitados
    # Realiza una búsqueda en anchura (BFS) en el grafo a partir del nodo inicial y devuelve una lista de nodos visitados.

def dfs(grafo, inicio, visitados=None):
    if visitados is None:
        visitados = []

    visitados.append(inicio)

    for vecino in grafo[inicio]:
        if vecino not in visitados:
            dfs(grafo, vecino, visitados)
    
    return visitados
    # Realiza una búsqueda en profundidad (DFS) en el grafo a partir del nodo inicial y devuelve una lista de nodos visitados.

def dijkstra(grafo, inicio):
    distancias = {nodo: float('infinity') for nodo in grafo}
    distancias[inicio] = 0
    pq = [(0, inicio)]

    while pq:
        distancia_actual, nodo_actual = heapq.heappop(pq)

        if distancia_actual > distancias[nodo_actual]:
            continue

        for vecino, peso in grafo[nodo_actual].items():
            distancia = distancia_actual + peso

            if distancia < distancias[vecino]:
                distancias[vecino] = distancia
                heapq.heappush(pq, (distancia, vecino))
    
    return distancias
    # Implementa el algoritmo de Dijkstra para encontrar las distancias más cortas desde un nodo inicial a todos los demás nodos en un grafo ponderado.

def visualizar_grafo(grafo):
    # Detectar si el grafo es ponderado y dirigido
    es_dirigido = any(isinstance(vecinos, dict) for vecinos in grafo.values())
    es_ponderado = es_dirigido and any(isinstance(peso, (int, float)) for vecinos in grafo.values() for peso in vecinos.values())

    # Crear el grafo adecuado
    G = nx.DiGraph() if es_dirigido else nx.Graph()

    for nodo in grafo:
        if es_ponderado:
            for vecino, peso in grafo[nodo].items():
                G.add_edge(nodo, vecino, weight=peso)
        else:
            for vecino in grafo[nodo]:
                G.add_edge(nodo, vecino)

    # Dibujar el grafo
    pos = nx.spring_layout(G)
    nx.draw(G, pos, with_labels=True, font_weight='bold')
    
    if es_ponderado:
        edge_labels = nx.get_edge_attributes(G, 'weight')
        nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels)
    
    plt.show()
    # Visualiza el grafo utilizando la librería NetworkX y Matplotlib. Detecta si el grafo es dirigido y/o ponderado y lo dibuja adecuadamente.

def menu_pilas():
    pila = Pila()
    while True:
        print("\nMenú de Pilas:")
        print("1. Apilar elemento")
        print("2. Desapilar elemento")
        print("3. Ver elemento en el tope")
        print("4. Verificar si la pila está vacía")
        print("5. Ver tamaño de la pila")
        print("6. Volver al menú principal")

        opcion = input("Seleccione una opción: ")
        
        if opcion == '1':
            elemento = input("Ingrese el elemento a apilar: ")
            pila.apilar(elemento)
        elif opcion == '2':
            print("Desapilando elemento:", pila.desapilar())
        elif opcion == '3':
            print("Elemento en el tope de la pila:", pila.tope())
        elif opcion == '4':
            print("¿Está vacía la pila?:", pila.esta_vacia())
        elif opcion == '5':
            print("Tamaño de la pila:", pila.tamano())
        elif opcion == '6':
            print("Volviendo al menú principal...")
            break
        else:
            print("Opción no válida. Intente de nuevo.")
    # Muestra un menú para interactuar con la pila, permitiendo apilar, desapilar, ver el tope, verificar si está vacía, y ver el tamaño de la pila.

def menu_colas():
    cola = Cola()
    while True:
        print("\nMenú de Colas:")
        print("1. Encolar elemento")
        print("2. Desencolar elemento")
        print("3. Verificar si la cola está vacía")
        print("4. Ver tamaño de la cola")
        print("5. Volver al menú principal")

        opcion = input("Seleccione una opción: ")
        
        if opcion == '1':
            elemento = input("Ingrese el elemento a encolar: ")
            cola.encolar(elemento)
        elif opcion == '2':
            print("Desencolando elemento:", cola.desencolar())
        elif opcion == '3':
            print("¿Está vacía la cola?:", cola.esta_vacia())
        elif opcion == '4':
            print("Tamaño de la cola:", cola.tamano())
        elif opcion == '5':
            print("Volviendo al menú principal...")
            break
        else:
            print("Opción no válida. Intente de nuevo.")
    # Muestra un menú para interactuar con la cola, permitiendo encolar, desencolar, verificar si está vacía, y ver el tamaño de la cola.

def menu_busqueda():
    grafo = {
        'A': ['B', 'C'],
        'B': ['A', 'D', 'E'],
        'C': ['A', 'F'],
        'D': ['B'],
        'E': ['B', 'F'],
        'F': ['C', 'E']
    }

    while True:
        print("\nMenú de Búsqueda:")
        print("1. BFS")
        print("2. DFS")
        print("3. Visualizar Grafo")
        print("4. Volver al menú principal")

        opcion = input("Seleccione una opción: ")
        
        if opcion == '1':
            inicio = input("Ingrese el nodo de inicio para BFS: ")
            print("Resultado de BFS:", bfs(grafo, inicio))
        elif opcion == '2':
            inicio = input("Ingrese el nodo de inicio para DFS: ")
            print("Resultado de DFS:", dfs(grafo, inicio))
        elif opcion == '3':
            visualizar_grafo(grafo)
        elif opcion == '4':
            print("Volviendo al menú principal...")
            break
        else:
            print("Opción no válida. Intente de nuevo.")
    # Muestra un menú para realizar búsquedas BFS y DFS en un grafo, así como para visualizar el grafo.

def menu_dijkstra():
    grafo = {
        'A': {'B': 1, 'C': 4},
        'B': {'A': 1, 'D': 2, 'E': 5},
        'C': {'A': 4, 'F': 3},
        'D': {'B': 2},
        'E': {'B': 5, 'F': 1},
        'F': {'C': 3, 'E': 1}
    }

    while True:
        print("\nMenú de Dijkstra:")
        print("1. Calcular Dijkstra")
        print("2. Visualizar Grafo")
        print("3. Volver al menú principal")

        opcion = input("Seleccione una opción: ")
        
        if opcion == '1':
            inicio = input("Ingrese el nodo de inicio para Dijkstra: ")
            print("Resultado de Dijkstra:", dijkstra(grafo, inicio))
        elif opcion == '2':
            visualizar_grafo(grafo)
        elif opcion == '3':
            print("Volviendo al menú principal...")
            break
        else:
            print("Opción no válida. Intente de nuevo.")
    # Muestra un menú para calcular el algoritmo de Dijkstra y visualizar el grafo.

def menu_arbol_binario():
    root = None
    while True:
        print("\nMenú de Árboles Binarios:")
        print("1. Insertar nodo")
        print("2. Buscar nodo")
        print("3. Mostrar recorrido en orden")
        print("4. Volver al menú principal")

        opcion = input("Seleccione una opción: ")
        
        if opcion == '1':
            llave = int(input("Ingrese el valor del nodo a insertar: "))
            if root is None:
                root = Nodo(llave)
            else:
                insertar(root, llave)
        elif opcion == '2':
            llave = int(input("Ingrese el valor del nodo a buscar: "))
            resultado = buscar(root, llave)
            if resultado:
                print(f"Nodo {llave} encontrado.")
            else:
                print(f"Nodo {llave} no encontrado.")
        elif opcion == '3':
            print("Recorrido en orden:", inorden(root))
        elif opcion == '4':
            print("Volviendo al menú principal...")
            break
        else:
            print("Opción no válida. Intente de nuevo.")
    # Muestra un menú para interactuar con un árbol binario de búsqueda (BST), permitiendo insertar nodos, buscar nodos y mostrar el recorrido en orden.

def mostrar_menu_principal():
    print("\n--------------TALLER DE RECUPERACION-----------")
    print("1. Búsqueda BFS y DFS")
    print("2. Árboles B+")
    print("3. Árboles binarios")
    print("4. Algoritmo Dijkstra")
    print("5. Pilas")
    print("6. Colas")
    print("7. Visualización")
    print("8. Salir")
    # Muestra el menú principal con las opciones disponibles.

def main():
    while True:
        mostrar_menu_principal()
        opcion = input("Seleccione una opción: ")
        
        if opcion == '1':
            menu_busqueda()
        elif opcion == '2':
            print("Funcionalidad de Árboles B+ no implementada.")
        elif opcion == '3':
            menu_arbol_binario()
        elif opcion == '4':
            menu_dijkstra()
        elif opcion == '5':
            menu_pilas()
        elif opcion == '6':
            menu_colas()
        elif opcion == '7':
            print("Funcionalidad de Visualización no implementada.")
        elif opcion == '8':
            print("Saliendo del programa...")
            break
        else:
            print("Opción no válida. Intente de nuevo.")
    # Ejecuta el menú principal y permite seleccionar opciones para interactuar con pilas, colas, árboles binarios, búsquedas BFS/DFS y el algoritmo de Dijkstra.

if __name__ == "__main__":
    main()
    # Punto de entrada del programa. Ejecuta la función main() para iniciar el menú principal.
